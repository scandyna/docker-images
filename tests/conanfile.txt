[requires]
qt/5.14.2@bincrafters/stable

[generators]
cmake
cmake_paths
virtualenv
