#include "QtWidgetsQtTest.h"
#include <QLineEdit>
#include <QApplication>
#include <QTimer>
#include <QFont>
#include <QFontDatabase>
#include <QDebug>
#include <iostream>

void showWidgetAndQuitApp(QWidget &widget)
{
  widget.show();
  QTimer::singleShot(20, &widget, &QWidget::hide);
  QTimer::singleShot(100, qApp, &QApplication::quit);
}

void QtWidgetsQtTest::showLineEditAndQuit()
{
  QLineEdit edit;
  showWidgetAndQuitApp(edit);
}

int main(int argc, char **argv)
{
  QApplication app(argc, argv);

  QFontDatabase fontDatabase;

  std::cout << "************************************\n";
  std::cout << "application font: " << app.font().toString().toStdString() << "\n";
  std::cout << "************************************" << std::endl;

  qDebug() << "************************************";
  qDebug() << "font families: " << fontDatabase.families();
  qDebug() << "************************************";

  QtWidgetsQtTest test;

  return QTest::qExec(&test, argc, argv);
}
