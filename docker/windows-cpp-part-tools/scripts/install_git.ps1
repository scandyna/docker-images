
echo "Downloading Git ..."
Invoke-WebRequest -Uri "https://github.com/git-for-windows/git/releases/download/v2.28.0.windows.1/Git-2.28.0-64-bit.exe" -OutFile "C:\TEMP\git_install.exe"

if(!$?){
  Write-Output "Downloading Git failed"
  exit 1
}


echo "Installing Git ..."
Start-Process -FilePath C:\TEMP\git_install.exe -ArgumentList "/VERYSILENT /LOADINF=C:\TEMP\install_git_config.ini" -NoNewWindow -Wait

if(!$?){
  Write-Output "Installing Git failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Git done"
