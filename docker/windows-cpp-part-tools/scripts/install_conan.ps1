
echo "Installing Conan ..."
pip install conan
conan config set general.revisions_enabled=1
conan remote add bincrafters https://bincrafters.jfrog.io/artifactory/api/conan/public-conan
# conan remote add gitlab https://gitlab.com/api/v4/projects/25668674/packages/conan

if(!$?){
  Write-Output "Installing Conan failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Conan done"
