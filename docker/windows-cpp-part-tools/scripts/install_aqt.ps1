
echo "Installing aqt (Another Qt installer) ..."
pip install aqtinstall

if(!$?){
  Write-Output "Installing aqt failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing aqt done"
