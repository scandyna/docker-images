
FROM ubuntu:18.04

# Need to install tzdata in non interactive mode
# See https://stackoverflow.com/questions/44331836/apt-get-install-tzdata-noninteractive
# and https://serverfault.com/questions/949991/how-to-install-tzdata-on-a-ubuntu-docker-image

# For locales, see jaredmarkell.com/docker-and-locales

RUN apt-get update -y \
  && export DEBIAN_FRONTEND=noninteractive \
  && ln -fs /usr/share/zoneinfo/Europe/Zurich /etc/localtime \
  && apt-get install -y tzdata \
  && dpkg-reconfigure --frontend noninteractive tzdata \
  && apt-get install locales -y \
  && locale-gen fr_CH.UTF-8

# Prepare to install recent versions of CMake in images that need it
RUN apt-get install -y apt-transport-https ca-certificates gnupg software-properties-common wget \
  && wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null \
  && apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main' \
  && apt-get update -y \
  && apt-get install kitware-archive-keyring \
  && rm /etc/apt/trusted.gpg.d/kitware.gpg

ENV LANG fr_CH.UTF-8
ENV LANGUAGE fr_CH:fr
