
Write-Output "Downloading VsCollect ..."
Invoke-WebRequest -Uri "https://aka.ms/vscollect.exe" -OutFile "C:\TEMP\vs_collect.exe"

if(!$?){
  Write-Output "Downloading VsCollect failed"
  exit 1
}


echo "Downloading MSVC ..."
Invoke-WebRequest -Uri "https://aka.ms/vs/16/release/vs_buildtools.exe" -OutFile "C:\TEMP\vs_buildtools.exe"

if(!$?){
  Write-Output "Downloading MSVC failed"
  exit 1
}


# TODO: add --lang en-US () For this, see layout option) ?

echo "Installing MSVC ..."
$Installprocess = Start-Process -FilePath C:\TEMP\vs_buildtools.exe -ArgumentList "--quiet --wait --norestart --nocache --add Microsoft.VisualStudio.Workload.VCTools --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 --add Microsoft.VisualStudio.Component.Windows10SDK.18362" -NoNewWindow -Wait -PassThru

if($Installprocess.ExitCode -ne 0){
  Write-Output "Installing MSVC failed, code: $($Installprocess.ExitCode)"

  Write-Output "Collecting install logs ..."
  Start-Process -FilePath C:\TEMP\vs_collect.exe -ArgumentList "-zip:C:\vs_install_logs.zip" -Wait
  Write-Output "Collecting install logs finished, file: C:\vs_install_logs.zip"

  exit $Installprocess.ExitCode
}

Write-Output "Installing MSVC done"


# See https://gitlab.com/scandyna/docker-images/-/issues/1
Write-Output "Mapping font MS Shell Dlg 2 ..."

#Set-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\FontSubstitutes" -Name "MS Shell Dlg 2" -Value "Microsoft Sans Serif"
Set-ItemProperty -Path Registry::"HKEY_LOCAL_MACHINE\Software\Microsoft\Windows NT\CurrentVersion\FontSubstitutes" -Name "MS Shell Dlg 2" -Value "Lucida Console"

if(!$?){
  Write-Output "Mapping font MS Shell Dlg 2 failed"
  exit 1
}

Write-Output "Mapping font MS Shell Dlg 2 done"
