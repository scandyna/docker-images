
echo "Installing Qt ..."
mkdir C:\Qt
# python -m aqt install --outputdir C:\Qt 5.14.2 windows desktop win64_mingw73
python -m aqt install-qt --outputdir C:\Qt windows desktop 5.14.2 win64_mingw73

if(!$?){
  Write-Output "Installing Qt failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Qt done"
