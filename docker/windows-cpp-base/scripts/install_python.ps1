
echo "Downloading Python ..."
Invoke-WebRequest -Uri "https://www.python.org/ftp/python/3.8.5/python-3.8.5.exe" -OutFile "C:\TEMP\python_install.exe"

if(!$?){
  Write-Output "Downloading Python failed"
  exit 1
}


echo "Installing Python ..."
Start-Process -FilePath C:\TEMP\python_install.exe -ArgumentList "/quiet InstallAllUsers=1 TargetDir=C:\tools\python3.8 PrependPath=1 Include_doc=0 Include_test=0" -NoNewWindow -Wait

if(!$?){
  Write-Output "Installing Python failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing Python done"
