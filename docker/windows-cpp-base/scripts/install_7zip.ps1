
echo "Downloading 7-Zip ..."
Invoke-WebRequest -Uri "https://www.7-zip.org/a/7z1900-x64.exe" -OutFile "C:\TEMP\7zip_install.exe"

if(!$?){
  Write-Output "Downloading 7-Zip failed"
  exit 1
}


echo "Installing 7-Zip ..."
Start-Process -FilePath C:\TEMP\7zip_install.exe -ArgumentList "/S" -NoNewWindow -Wait
setx PATH "%PATH%;C:\Program Files\7-Zip"

if(!$?){
  Write-Output "Installing 7-Zip failed, code $LASTEXITCODE"
  exit 1
}
Write-Output "Installing 7-Zip done"
