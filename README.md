# docker-images

Some Docker images used for GitLab CI/CD

# Usage for Linux

Here is a minimal example for Linux:
```yml
stages:
  - build

build_linux_gcc_debug:
  stage: build
  image: registry.gitlab.com/scandyna/docker-images/ubuntu-18.04-cpp-gui:latest
  script:
    - mkdir build
    - cd build
    - conan install -s build_type=Debug --build=missing ..
    - cmake -G"Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
    - make -j4
  artifacts:
    expire_in: 1 day
    paths:
      - build
```

To run tests that requires a X11 server running,
choose a image that has the required packages installed,
and launch the [Xdummy](https://xpra.org/trac/wiki/Xdummy) server:
```yml
stages:
  - test

test_linux_gcc_debug:
  stage: test
  image: registry.gitlab.com/scandyna/docker-images/ubuntu-18.04-cpp-gui:latest
  variables:
    DISPLAY: ":99"
  dependencies:
    - build_linux_gcc_debug
  before_script:
    - /etc/init.d/xdummy-server start
  script:
    - cd build
    - ctest --output-on-failure .
```

## Available Linux images

### ubuntu-18.04-minimal

A minimal image based on [ubuntu 18.04](https://hub.docker.com/_/ubuntu)
with some additions, like UTF-8 locale.

This image does not provide any libraries like Qt5,
and can be used to test deployed applications.

### ubuntu-18.04-cpp

Based on `ubuntu-18.04-minimal`,
this image provides compilers, libraries and tools to build and test C++ software.

### ubuntu-18.04-cpp-qt-5.14.2-core-clang6.0-tsan

Based on `ubuntu-18.04-minimal`,
this image provides Qt 5.14.2 compiled with Clang, libc++ and Thread sanitizer.

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images/ubuntu-18.04-cpp-qt-5.14.2-core-clang6.0-tsan:latest
```

This Qt version is installed with Conan.
In your conanfile, add `qt/5.14.2@bincrafters/stable` as requied.
To compile:
```bash
conan install --profile linux_clang6.0_x86_64_libc++_tsan_qt_core_modules -s build_type=RelWithDebInfo ..
source activate.sh
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DSANITIZER_ENABLE_THREAD=ON ..
```

The conan profiles are available [here](https://gitlab.com/scandyna/conan-config).

### ubuntu-18.04-minimal-xserver

Based on `ubuntu-18.04-minimal`,
this image has a `X server` installed to provide GUI support.
Helper scripts to start a dummy X server are also provided.

This image does not provide any libraries like Qt5,
and can be used to test deployed applications.

### ubuntu-18.04-cpp-gui

Based on `ubuntu-18.04-minimal-xserver`,
this image provides compilers, libraries and tools to build and test C++ software.

### ubuntu-18.04-cpp-gui-qt-5.14.2

Based on `ubuntu-18.04-minimal-xserver`,
this image provides compilers, libraries and tools to build and test C++ software.

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images/ubuntu-18.04-cpp-gui-qt-5.14.2:latest
```

Qt 5.14.2 is installed in `/opt/Qt/5.14.2/gcc_64`.


### ubuntu-18.04-cpp-qt-widgets-conan-base

Based on `ubuntu-18.04-minimal-xserver`,
this image provides the basis for Qt Widgets Conan based images.

### ubuntu-18.04-cpp-qt-5.14.2-widgets-clang6.0-debug

Based on `ubuntu-18.04-cpp-qt-widgets-conan-base`,
this image provides Qt 5.14.2 compiled with Clang, libc++.

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images/ubuntu-18.04-cpp-qt-5.14.2-widgets-clang6.0-debug:latest
```

This Qt version is installed with Conan.
In your conanfile, add `qt/5.14.2@bincrafters/stable` as requied.
To compile:
```bash
conan install --profile linux_clang6.0_x86_64_libc++_qt_widgets_modules -s build_type=Debug ..
source activate.sh
cmake -DCMAKE_BUILD_TYPE=Debug ..
```

### ubuntu-18.04-cpp-qt-5.14.2-widgets-clang6.0-release

Based on `ubuntu-18.04-cpp-qt-widgets-conan-base`,
this image provides Qt 5.14.2 compiled with Clang, libc++.

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images/ubuntu-18.04-cpp-qt-5.14.2-widgets-clang6.0-release:latest
```

This Qt version is installed with Conan.
In your conanfile, add `qt/5.14.2@bincrafters/stable` as requied.
To compile:
```bash
conan install --profile linux_clang6.0_x86_64_libc++_qt_widgets_modules -s build_type=Release ..
source activate.sh
cmake -DCMAKE_BUILD_TYPE=Release ..
```


### ubuntu-18.04-cpp-qt-5.14.2-widgets-clang6.0-tsan

Based on `ubuntu-18.04-cpp-qt-widgets-conan-base`,
this image provides Qt 5.14.2 compiled with Clang, libc++ and Thread sanitizer.

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images/ubuntu-18.04-cpp-qt-5.14.2-widgets-clang6.0-tsan:latest
```

This Qt version is installed with Conan.
In your conanfile, add `qt/5.14.2@bincrafters/stable` as requied.
To compile:
```bash
conan install --profile linux_clang6.0_x86_64_libc++_tsan_qt_widgets_modules -s build_type=RelWithDebInfo ..
source activate.sh
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DSANITIZER_ENABLE_THREAD=ON ..
```

The conan profiles are available [here](https://gitlab.com/scandyna/conan-config).


# Usage for Windows

Currently (September 2020), GitLab.com does provide Windows shared runners,
but they do not support running docker images.
More information is available [here](https://about.gitlab.com/blog/2020/01/21/windows-shared-runner-beta/).

The first step is to install and register a runner,
like explained in the [GitLab doc](https://docs.gitlab.com/runner/install/windows.html).
Also register a runner with a [docker-windows](https://docs.gitlab.com/runner/executors/docker.html)
executor, and give him a tag that you remember (for example `windows-mypc-docker`).

Then a minimal example for Windows:
```yml
.windows_runner:
  tags:
    - windows-mypc-docker

stages:
  - build
  - test

build_windows_gcc_debug:
  stage: build
  image: registry.gitlab.com/scandyna/docker-images/windows-cpp-qt-5.14.2-win64_mingw73:latest
  extends: .windows_runner
  script:
    - mkdir build
    - cd build
    - cmake -G"MinGW Makefiles" -DCMAKE_BUILD_TYPE=Debug
            -DCMAKE_PREFIX_PATH="C:/Libraries/boost_1_73_0;C:/Qt/5.14.2/mingw73_64" ..
    - cmake --build . --config Debug -j4
  artifacts:
    expire_in: 1 day
    paths:
      - build

test_windows_gcc_debug:
  stage: test
  image: registry.gitlab.com/scandyna/docker-images/windows-cpp-qt-5.14.2-win64_mingw73:latest
  extends: .windows_runner
  dependencies:
    - build_windows_gcc_debug
  before_script:
    - $env:PATH += ";C:/Qt/5.14.2/mingw73_64/bin"
  script:
    - cd build
    - ctest --output-on-failure --build-config Debug .
```

Note: in above example we added the Qt bin directory
to the PATH, so the loader can find Qt's dll's to run the tests.
A possible alternative could be to handle those dll problems with CMake.
For example, use [MdtAddTest](https://scandyna.gitlab.io/mdt-cmake-modules/Modules/MdtAddTest.html),
which handle that as exaplained in the [MdtRuntimeEnvironment](https://scandyna.gitlab.io/mdt-cmake-modules/Modules/MdtRuntimeEnvironment.html) module.

## Available Windows images

Some tools are available in each image.

Python >= 3.8.5 is installed, and in the `PATH`.

7-Zip is installed, and in the `PATH`.

CMake is installed, and in the `PATH`.

Conan is installed, and can be called directly.

Git is installed, and in the `PATH`.

Boost 1.73 is installed in `C:\Libraries\boost_1_73_0`.


### windows-cpp-qt-5.14.2-win64_mingw73

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images/windows-cpp-qt-5.14.2-win64_mingw73:latest
```

gcc 7.3 64bit is installed in `C:\Qt\Tools\mingw730_64\bin` and is in the `PATH`.

Qt 5.14.2 is installed in `C:\Qt\5.14.2\mingw73_64`.

### windows-cpp-qt-5.14.2-win32_mingw73

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images/windows-cpp-qt-5.14.2-win32_mingw73:latest
```

gcc 7.3 32bit is installed in `C:\Qt\Tools\mingw730_32\bin` and is in the `PATH`.

Qt 5.14.2 is installed in `C:\Qt\5.14.2\mingw73_32`.

### windows-cpp-qt-5.14.2-win64_msvc2017_64

Specify the image:
```yml
image: registry.gitlab.com/scandyna/docker-images/windows-cpp-qt-5.14.2-win64_msvc2017_64:latest
```

MSVC 2019 is default installed.
CMake should be able to find it.

Qt 5.14.2 is installed in `C:\Qt\5.14.2\msvc2017_64`


# Note about the pipeline to create Windows images

The pipeline to create Windows images seems somewhat complex.
It has a lot of stages, and this for reasons described here.

The first problem is to reuse tools after they have been installed.
For example, Python is a base requirement, used by conan and aqt.
Once installed, the shell has to be restarted to have Python in the PATH.
I simply did'n find a solution for that, so I choosed to create a image,
name `windows-cpp-base`.
This image alos contains `7-Zip`, required later.

A other problem was to be able to build the images using GitLab.com.
With a image containing almost everything just did not work,
because it took more than 1 hour to complete.
It is possible to change the jobs timeout for a project,
but then, the Windows shared runner imposes the 1 hour timeout.

Windows shared Windows runners have also disk space limits,
for example: https://gitlab.com/scandyna/docker-images/-/jobs/735979350

This are the reason why there are som many images, and also build jobs.

Note: finally, using Windows shared runners not work for some jobs,
typically for the `windows-cpp-part-msvc` image, which takes to long.

## Windows runner setup to build images

In the directoy where `gitlab-runner.exe` lives,
a configuration file is available: `config.toml`.

It is possible to allow the runner to take multiple jobs:
```
concurrent = 1
```

To create the Docker images, I use a `shell` executor (no DinD available for Windows).

In such case, only 1 job should be allowed.
Otherwise, 1 job could do a `docker login`,
while a other trys to take image from the registry, which will fail.

More about runner configuration is available
in the [documentation](https://docs.gitlab.com/runner/configuration/advanced-configuration.html).

## Save disk space

Having y tiny SSD on my laptop, available disk space shrinked very fast.

Cleaning up Docker's caches helped:
`docker system prune`

When not enough, all images could be removed:
`docker rmi $(docker images -q)`

More info [here](https://docs.docker.com/config/pruning/).

# Rationale

## CI/CD on Windows

For Windows, [AppVeyor](https://www.appveyor.com/) is what I use so far.
It provides compilers, tools and libraries to create C++/Qt SW.

I personally prefer GitLab CI/CD, because lets you run several jobs in //
without spending a lot of money.
I also prefer the concept of jobs and pipeline,
and the description language (.gitlab-ci.yml) is more flexible
in GitLab, in my opinion.

Because GitLab now beginns to provide Windows runners, I decided to try it.

I would like to also use a Docker image to build and test the SW.
Here, the journey begins.

### Create Docker images for Windows

The first step is to be able to create images with the various
tools, compilers and libraries.
After sandboxing with my personal laptop (learning Docker, Powershell),
I had a minimal Dockerfile and scripts to install Python (offline).

Creating the Docker image on GitLab with a shared Windows runner
seemed to work (docker is installed in the VM), but the test job not.
[The documentation](https://about.gitlab.com/blog/2020/01/21/windows-shared-runner-beta/)
explains that current Windows shared runner do not provide Docker executor.

I then installed GitLab Runner on my laptop as explained [here](https://docs.gitlab.com/runner/install/windows.html),
registred as a [docker-windows](https://docs.gitlab.com/runner/executors/docker.html) executor.

This time, the test job launched the image created during the build job,
but Python was not installed.
I then registered a second runner on my laptop with a [shell executor](https://docs.gitlab.com/runner/executors/shell.html).
Then, after working around a problem (described after), it seemed to work, for some unknown reason (Python was installed), but see below..

When the build job and the test job runs on my laptop,
the test job failed like [this](https://gitlab.com/scandyna/docker-images/-/jobs/735120235).

This could be workaround by removing `C:\Windows\System32\config\systemprofile\.docker\config.json`
before the test jobs starts.

After a while, I submitted a comment to [issue 40925](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/40925).

Finally this has nothing to to with any issue, I simply did the docker logout wrong, omitting to specify the registry.

Having again spourious problem installing Python,
I finally realised that my script was wrong.
Calling `C:\TEMP\python_install.exe /quiet` is a async call (i.e. the install started in baground).

After fixing that, I came back to build the image on a GitLab Windows shared runner,
then test with the runner installed on my laptop.

In between, I also explored AWS solutions.
Creating a EC2 instance with a Windows 2019 server with docker installed was easy.
But, leting the instance running will cost me to much.

So, how can this be hanlded ?

First try: [Autoscaling GitLab Runner on AWS EC2](https://docs.gitlab.com/runner/configuration/runner_autoscale_aws/).
But, this will launch a Docker image, which requires DinD to build images, which is not supported on Windows.
See also [issue 4338](https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4338).

Second try: use Kubernetis.
This requires a runner that is up 24/7, so back again to the cost problem.
Also, Kubernetis is overkill for my use case.
