
# Qt on Linux

qtdiag fails by default while trying to load xcb plugin.

If setting `QT_QPA_PLATFORM=linuxfb` or `QT_QPA_PLATFORM=minimal` it works.

Should test what works for applications requiring Gui (also Widgets).

The env vars QT_QPA_PLATFORM=linuxfb , should it be put in the docker images ?
Is DISPLAY=99 meaningful ?
Should xserver (dummy) be started in the image ?

# Conan

conan profile update settings.compiler.libcxx=libstdc++11 default

# Boost

Should we also provide installed conan packages in images ?

# windows-cpp images

For Qt and boost, which does not have wired things (like regitry changes during install, I think),
we could use multi-stage build ?
```
FROM some-base-image
COPY --from some-image ...
```

# Images tags while build / test

Try out $CI_COMMIT_REF_SLUG


# Git strategy

see:
```yml
variables:
 - GIT_STRATEGY: none
```
(Not clone the git repo, for tests..)
